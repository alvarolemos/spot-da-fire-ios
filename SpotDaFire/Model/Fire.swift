import Foundation
import MapKit

final class Fire: NSObject, Codable {
    var name: String?
    let latitude: Double
    let longitude: Double
    let kind: Kind?
    let date: Date?
    let distance: Double
    let severity: Severity?
    let cluster: String
    let color: String?


    init(json: [String: Any],
         userLocation: CLLocation) {
        self.latitude = json["latitude"] as! Double
        self.longitude = json["longitude"] as! Double
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = json["acq_date"] as? String {
            self.date = dateFormatter.date(from: date)
        } else {
            self.date = nil
        }

        self.name = "Loading..."
        self.kind = .nasa
        let location = CLLocation(latitude: latitude,
                                  longitude: longitude)
        self.distance = userLocation.distance(from: location) / 1000
        self.severity = Severity(code: 0)

        if let cluster = json["cluster"] as? String {
            self.cluster = cluster
        } else if let cluster = json["cluster"] as? Int {
            self.cluster = "\(cluster)"
        } else {
            self.cluster = "cluster"
        }
        if let color = json["color"] as? String {
            self.color = color
        } else {
            switch self.cluster {
            case "High":
                self.color = "e26f38"
            case "Moderate":
                self.color = "ed9e42"
            case "Extreme":
                self.color = "dd4a42"
            default:
                self.color = "e26f38"
            }
        }
    }

    enum Kind: String, Codable {
        case nasa
        case user

        var color: UIColor {
            return self == .nasa ? .red : .yellow
        }

        var image: UIImage {
            return self == .nasa ? UIImage(named: "first")! : UIImage(named: "second")!
        }
    }

    struct Severity: Codable {
        let code: Int
    }

    init(name: String = "",
         latitude: Double = 0.0,
         longitude: Double = 0.0,
         severity: Severity,
         kind: Kind = .nasa,
         date: Date,
         userLocation: CLLocation) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.kind = kind
        self.severity = severity
        self.date = date
        let location = CLLocation(latitude: latitude,
                                  longitude: longitude)
        self.distance = userLocation.distance(from: location) / 1000
        self.cluster = "cluster"
        self.color = ""
    }
}

extension Fire: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude,
                                      longitude: longitude)
    }

    var subtitle: String? {
        return "wildfire".localized
    }

    var title: String? {
        return name
    }
}

extension Fire {
    enum Resource: HTTPResource {
        case all

        var path: String {
            return ""
        }
    }
}
