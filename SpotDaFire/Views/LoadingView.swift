import UIKit

class LoadingView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = 5
        self.addSubview(containerView)
        self.containerView.addSubview(indicator)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private lazy var containerView: UIView = self.makeContainverView()
    private func makeContainverView() -> UIView {
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.layer.cornerRadius = 5
        containerView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.9)
        return containerView
    }

    private lazy var indicator: UIActivityIndicatorView = self.makeIndicator()
    private func makeIndicator() -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.startAnimating()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        return indicator
    }

    func show(on viewController: UIViewController) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            viewController.view.addSubview(self)
            NSLayoutConstraint.activate([
                self.indicator.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor),
                self.indicator.centerYAnchor.constraint(equalTo: self.containerView.centerYAnchor),
                self.containerView.heightAnchor.constraint(equalToConstant: self.containerView.frame.size.height),
                self.containerView.widthAnchor.constraint(equalToConstant: self.containerView.frame.size.width),
                self.containerView.centerXAnchor.constraint(equalTo: viewController.view.centerXAnchor),
                self.containerView.centerYAnchor.constraint(equalTo: viewController.view.centerYAnchor),
                self.topAnchor.constraint(equalTo: viewController.view.topAnchor, constant: 0),
                self.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor, constant: 0),
                self.leftAnchor.constraint(equalTo: viewController.view.leftAnchor, constant: 0),
                self.rightAnchor.constraint(equalTo: viewController.view.rightAnchor, constant: 0)
            ])
        }
    }

    func hide() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.removeFromSuperview()
        }
    }
}
