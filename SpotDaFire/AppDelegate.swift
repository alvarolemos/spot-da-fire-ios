//
//  AppDelegate.swift
//  SpotDaFire
//
//  Created by Jean Sandrin on 19/10/18.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        GIDSignIn.sharedInstance().clientID = "660572640089-r99s976il8u6a3ulho2ilki8qagda0lh.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self


        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window

        let list = ListViewController()
        let navigation = UINavigationController(rootViewController: list)
        navigation.title = "List"
        navigation.tabBarItem = UITabBarItem(title: "List", image: UIImage(named: "second"), selectedImage: nil)
        let tabBar = UITabBarController()
        let map = MapViewController()
        map.title = "Map"
        map.tabBarItem = UITabBarItem(title: "Map", image: UIImage(named: "first"), selectedImage: nil)

        let nav = UINavigationController()
        let reportVC = ReportViewController()
        reportVC.title = "Report a Fire"
        nav.viewControllers = [reportVC]
        nav.tabBarItem = UITabBarItem(title: "Report", image: UIImage(named: "ic_camera"), tag: 0)
        tabBar.viewControllers = [map, nav]
        self.window?.rootViewController = tabBar
        self.window?.makeKeyAndVisible()

        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        return true
    }

    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance()!.handle(url,
                                                  sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            // ...
        }
    }
}

