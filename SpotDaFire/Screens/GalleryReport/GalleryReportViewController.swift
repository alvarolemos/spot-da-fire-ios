//
//  GalleryReportViewController.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 20/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Photos

struct ImageInfo {
    var image: UIImage
    var imageLocation: CLLocation
    var imageHour: Date
}

class GalleryReportViewController: UIViewController {


    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleInpuText: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var placeName: UILabel!

    let imagePicker = UIImagePickerController()
    
    var imageInfo: ImageInfo?
    

    lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        PHPhotoLibrary.requestAuthorization { status in
            switch status {
            case .authorized:
                self.showPicker()
            default:
                break
            }
        }
    }
    
    func showPicker() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen
            DispatchQueue.main.async {
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }

    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func report(_ sender: UIButton) {
        guard let imageInfo = imageInfo else {
            return
        }
        
        let lat = imageInfo.imageLocation.coordinate.latitude
        let lng = imageInfo.imageLocation.coordinate.longitude
        
        let date = Int(imageInfo.imageHour.timeIntervalSince1970)
        
        loadingView.show(on: self)
        imageInfo.image.uploadImagePic(latLng: (lat,lng), currentTime: "\(date)") { (url) in
            self.loadingView.hide()
                        guard let url = url else {
                            return
                        }
                        debugPrint(url)
            self.
            self.present(ReportedHelpedViewController(), animated: true, completion: nil)
        }
    }

    @IBAction func selectImage(_ sender: UIButton) {
        showPicker()
    }
}

extension GalleryReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @objc internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
        guard let image = info[.originalImage] as? UIImage else { return }

        self.imageView.image = image

        if let asset = info[.phAsset] as? PHAsset,
            let location = asset.location,
            let date = asset.creationDate {

            let locValue: CLLocationCoordinate2D = location.coordinate

            self.mapView.mapType = MKMapType.standard

            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: locValue, span: span)
            self.mapView.setRegion(region, animated: true)

            let annotation = MKPointAnnotation()
            annotation.coordinate = locValue
            annotation.title = "Fire!"
            self.mapView.addAnnotation(annotation)

            CLGeocoder().reverseGeocodeLocation(location) { (placeMarks, error) in
                if let _ = error {

                    return
                }
                self.placeName.text = placeMarks?[0].name
            }
            
            self.imageInfo = ImageInfo.init(image: image,
                                            imageLocation: location,
                                            imageHour: date)
        }
    }
}

extension GalleryReportViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let view = FireAnnotation(annotation: annotation,
                                  reuseIdentifier: FireAnnotation.reuseID)
        view.canShowCallout = true
        return view
    }
}
