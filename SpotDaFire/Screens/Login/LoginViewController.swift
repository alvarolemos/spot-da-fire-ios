import UIKit
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {


    var googleSiginButton: GIDSignInButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self

        googleSiginButton = GIDSignInButton(frame: .zero)
        googleSiginButton?.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(googleSiginButton)
        NSLayoutConstraint.activate([
            googleSiginButton.heightAnchor.constraint(equalToConstant: 50),
            googleSiginButton.widthAnchor.constraint(equalToConstant: 200),
            googleSiginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            googleSiginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])

        googleSiginButton.addTarget(self, action: #selector(sigin), for: .allEvents)
    }

    @objc func sigin() {

    }
}


extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        dismiss(animated: true, completion: nil)
    }
}
