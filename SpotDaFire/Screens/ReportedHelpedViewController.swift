//
//  ReportedHelpedViewController.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 21/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit

class ReportedHelpedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
    }


    @IBAction func close(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

}
