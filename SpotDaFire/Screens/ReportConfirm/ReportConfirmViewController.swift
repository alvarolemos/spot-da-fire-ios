//
//  ReportConfirmViewController.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 20/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Photos
import FirebaseStorage
import GoogleSignIn

class ReportConfirmViewController: UIViewController {
    
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleInpuText: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var placeName: UILabel!
    
    
    lazy var loading = LoadingView()
    
    var image: UIImage?
    var imageInfo: ImageInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = image
        
        // For use in foreground
        self.locationManager = CLLocationManager()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        
        PHPhotoLibrary.requestAuthorization { status in
            
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        guard let image = self.image,
            let location = self.currentLocation else {
                return
        }
        PHPhotoLibrary.shared().performChanges({
            // Request creating an asset from the image.
            let creationRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            // Set metadata location
            creationRequest.location = location
        }, completionHandler: { success, error in
            self.navigationController?.presentingViewController?.dismiss(animated: true, completion: nil)
            if !success { NSLog("error creating asset: \(error!)") }
        })
    }
    
    @IBAction func report(_ sender: UIButton) {
        guard let imageInfo = imageInfo else {
            return
        }
        
        let lat = imageInfo.imageLocation.coordinate.latitude
        let lng = imageInfo.imageLocation.coordinate.longitude
        
        let date = Int(imageInfo.imageHour.timeIntervalSince1970)
        
        loading.show(on: self)
        
        imageInfo.image.uploadImagePic(latLng: (lat,lng), currentTime: "\(date)") { (url) in
            self.loading.hide()
            guard let url = url else {
                return
            }
            debugPrint(url)
            self.navigationController?.pushViewController(ReportedHelpedViewController(), animated: true)
        }
        
    }
    
}

extension ReportConfirmViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let view = FireAnnotation(annotation: annotation,
                                  reuseIdentifier: FireAnnotation.reuseID)
        view.canShowCallout = true
        return view
    }
}


extension ReportConfirmViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = manager.location
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        mapView.mapType = MKMapType.standard
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = locValue
        annotation.title = "Fire!"
        mapView.addAnnotation(annotation)
        
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placeMarks, error) in
            if let _ = error {
                
                return
            }
            
            self.placeName.text = placeMarks?[0].name
        }
        
        
        self.imageInfo = ImageInfo.init(image: self.image!, imageLocation: manager.location!, imageHour: Date())
        
        
        self.locationManager?.delegate = nil
    }
}


