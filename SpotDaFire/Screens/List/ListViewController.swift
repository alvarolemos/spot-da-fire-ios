import UIKit
import CoreLocation

class ListViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    let userLocation = CLLocationCoordinate2D(latitude: -19.9, longitude: -43.9)
    lazy var interactor = ListInteractor(viewController: self)

    var dataSource: [Fire] = []
    var filteredSource: [Fire] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "list".localized
        navigationItem.searchController = createSearchController()
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .organize, target: self, action: #selector(filter))
        navigationItem.rightBarButtonItem = rightBarButton
        registerCells()
        interactor.getAll()
    }

    private func registerCells() {
        let nib = UINib(nibName: String(describing: ListTableViewCell.self),
                        bundle: .main)
        tableView.register(nib, forCellReuseIdentifier: "listCellIdentifier")
    }

    @objc private func filter()  {
        let actionSheet = UIAlertController(title: "sort".localized,
                                            message: "Sort all the things!",
                                            preferredStyle: .actionSheet)

        let distance = UIAlertAction(title: "distance".localized,
                                     style: .default) { [unowned self] (action) in
                                        self.filteredSource = self.dataSource.sorted { first, second in return first.distance < second.distance }
                                        self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: .bottom)
        }

        let time = UIAlertAction(title: "time".localized,
                                 style: .default) { [unowned self] (action) in
                                    self.filteredSource = self.dataSource.sorted(by: { (first, second) -> Bool in
                                        let firstTimeInterval = first.date!.timeIntervalSince(Date())
                                        let secondTimeInterval = second.date!.timeIntervalSince(Date())
                                        return firstTimeInterval > secondTimeInterval
                                    })
                                    self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: .bottom)
        }

        let name = UIAlertAction(title: "Name".localized,
                                 style: .default) { [unowned self] _ in
                                    self.filteredSource = self.dataSource.sorted(by: { (first, second) -> Bool in
                                        first.name! < second.name!
                                    })

                                    self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: .bottom)
        }

        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil)
        actionSheet.addAction(distance)
        actionSheet.addAction(time)
        actionSheet.addAction(name)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }

    private func createSearchController() -> UISearchController {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        return searchController
    }

    func updateList(with fire: [Fire]) {
        dataSource = fire
        filteredSource = dataSource
        tableView.reloadData()
    }

    func show(_ message: String) {
        let alert = UIAlertController(title: "",
                                      message: message,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "OK",
                                   style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert,
                     animated: true,
                     completion: nil)
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "listCellIdentifier")
            as? ListTableViewCell else {
                return UITableViewCell()
        }

        let fire = filteredSource[indexPath.row]
        cell.cityLabel.text = fire.name

        if let time = fire.date {
            cell.timeLabel.text = "\(time.timeAgoSinceNow) ago"
        }

        let distance = String(format: "%.2f", fire.distance)
        cell.distanceLabel.text = "\(distance) km away"
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return filteredSource.count
    }
}

extension ListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text,
            !text.isEmpty else {
                filteredSource = dataSource
                return
        }
        switch searchController.searchBar.selectedScopeButtonIndex {
        case 0:
            filteredSource = dataSource.filter { $0.name!.contains(text) }
        case 1:
            break
        default:
            break
        }
    }
}
