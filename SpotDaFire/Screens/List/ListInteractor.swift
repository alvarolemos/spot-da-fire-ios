import Foundation
import then
import MapKit

final class ListInteractor {
    let client: HTTPClient
    weak var viewController: ListViewController?
    lazy var dataSource: [Fire] = []

    init(viewController: ListViewController,
         client: HTTPClient = APIClient()) {
        self.viewController = viewController
        self.client = client
    }

    func getAll() {
        if !Store.shared.dataSource.isEmpty {
            viewController?.updateList(with: Store.shared.dataSource)
            return
        }

        
        return
        let promise: Promise<[Fire]> = client.request(Fire.Resource.all)
        promise.then { [weak self] fires in
            DispatchQueue.main.async {
                self?.viewController?.updateList(with: fires)
            }
            }.onError { [weak self] error in
                DispatchQueue.main.async {
                    self?.viewController?.show(error.localizedDescription)
                }
        }
    }
}
