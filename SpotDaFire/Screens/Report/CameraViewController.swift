//
//  CameraViewController.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 20/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class CameraViewController: UIViewController {

    @IBOutlet weak var cameraContainer: UIView!
    @IBOutlet weak var buttonsContainer: UIView!
    @IBOutlet weak var authorizationView: UIView!
    
    
    @IBOutlet weak var takePictureButton: UIButton!
    
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var authorizationButton: UIButton!
    
    var cameraController: CameraController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        takePictureButton.layer.borderColor = UIColor.init(white: 55/255, alpha: 0.4).cgColor
        takePictureButton.layer.borderWidth = 2
        checkCameraPermission()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initCamera() {
        self.cameraController = CameraController()
        self.authorizationView.isHidden = true
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }
            
            try? self.cameraController.displayPreview(on: self.cameraContainer)
            self.cameraController.flashMode = .off
            self.flashButton.isSelected = false
        }
    }
    
    
    func checkCameraPermission() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.initCamera()
        } else {
            self.authorizationView.isHidden = false
        }
    }
    
    @IBAction func takePictureAction(_ sender: UIButton) {
        guard cameraController.currentCameraPosition != nil else {
            let imagepreview = ImagePreviewViewController()
            imagepreview.image = #imageLiteral(resourceName: "tempimage")
            self.navigationController?.pushViewController(imagepreview, animated: true)
            return
        }
        
        
        
        sender.isEnabled = false
        cameraController.captureImage {(image, error) in
            sender.isEnabled = true
            guard let image = image?.pngData() else {
                print(error ?? "Image capture error")
                return
            }
            
            let imagepreview = ImagePreviewViewController()
            imagepreview.image = UIImage.init(data: image)!
            self.navigationController?.pushViewController(imagepreview, animated: true)
        }
    }
    
    @IBAction func touchFlash(_ sender: UIButton) {
        if sender.isSelected {
            self.cameraController.flashMode = .off
            self.flashButton.isSelected = false
        } else {
            self.cameraController.flashMode = .on
            self.flashButton.isSelected = true
        }
    }
    
    
    @IBAction func authorizeAction(_ sender: UIButton) {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
            if granted {
                DispatchQueue.main.async {
                    self.initCamera()
                }
                
            } else {
                //access denied
            }
        })
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}


extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}
