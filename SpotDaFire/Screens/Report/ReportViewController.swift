//
//  ReportViewController.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 20/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func action(_ sender: UIButton) {
        switch sender.titleLabel?.text?.uppercased() {
        case .some("CAMERA"):
            let cameraVC = CameraViewController()
            let navController = UINavigationController.init()
            navController.viewControllers = [cameraVC]
            self.navigationController?.present(navController, animated: true)
        case .some(" GALLERY"):
            let galleryVC = GalleryReportViewController()
            //galleryVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(galleryVC, animated: true)
        default:
            break
        }
    }

}
