//
//  ImagePreviewViewController.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 20/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit

class ImagePreviewViewController: UIViewController {

    var image: UIImage!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " "
        imageView.image = image
    }


    @IBAction func confirmAction(_ sender: UIButton) {
        let vc = ReportConfirmViewController()
        vc.image = image
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
