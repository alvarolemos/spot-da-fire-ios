import UIKit
import MapKit
import GoogleSignIn

class MapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var infoButton: UIButton!
    lazy var loading = LoadingView(frame: .zero)

    var dataSource: [Fire] = []
    var filteredSource: [Fire] = []
    let userLocation = CLLocationCoordinate2D(latitude: 19.9, longitude: 43.9)
    lazy var interactor: MapInteractor = MapInteractor(viewController: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        registerAnnotations()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signInSilently()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.getAll(basedOn: mapView.userLocation)
    }

    @IBAction func didTapInfoIcon(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Map Settings",
                                            message: nil,
                                            preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: "Standard",
                                   style: .default) { [unowned self] _ in
                                    self.interactor.get(.fires, userLocation: self.mapView.userLocation)
        }

        let density = UIAlertAction(title: "Density",
                                   style: .default) { [unowned self] _ in
                                    self.interactor.get(.density, userLocation: self.mapView.userLocation)
        }

        let brightness = UIAlertAction(title: "Brightness",
                                       style: .default) { [unowned self] _ in
                                        self.interactor.get(.brightness, userLocation: self.mapView.userLocation)
        }


        actionSheet.addAction(action)
        actionSheet.addAction(brightness)
        actionSheet.addAction(density)
        present(actionSheet, animated: true, completion: nil)
    }

    private func registerAnnotations() {
        mapView.register(FireAnnotation.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        if #available(iOS 12.0, *) {
            mapView.register(FireClusterAnnotation.self,
                             forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        }
    }
}

extension MapViewController {
    func updateMap(with fires: [Fire]) {
        dataSource = fires
        filteredSource = dataSource
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(filteredSource)
        mapView.showAnnotations(filteredSource, animated: true)
    }

    func show(_ message: String) {
        let alert = UIAlertController(title: "",
                                      message: message,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "OK",
                                   style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert,
                     animated: true,
                     completion: nil)
    }

    func setMapRegion(_ region: MKCoordinateRegion) {
        mapView.setRegion(region, animated: true)
    }

    func showLoading() {
        loading.show(on: self)
    }

    func hideLoading() {
        loading.hide()
    }
}

extension MapViewController: MKMapViewDelegate, UISearchBarDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Fire {
            let view = FireAnnotation(annotation: annotation,
                                      reuseIdentifier: FireAnnotation.reuseID)
            view.canShowCallout = true
            return view
        }
        return nil
    }

    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            return
        }

        mapView.annotations.forEach {
            if $0 is MKUserLocation {
                return
            }
            let annotationView = mapView.view(for: $0)
            if annotationView != view {
                annotationView?.isHidden = true
            }
        }
    }

    func mapView(_ mapView: MKMapView,
                 didDeselect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            return
        }

        mapView.annotations.forEach {
            mapView.view(for: $0)?.isHidden = false
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !interactor.findCoordinate(for: searchBar.text) {
            filteredSource = dataSource
        }
        view.endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredSource = dataSource
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotations(self.filteredSource)
            self.mapView.showAnnotations(self.filteredSource, animated: true)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        searchBar.resignFirstResponder()
    }
}

extension MapViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            self.present(LoginViewController(), animated: true, completion: nil)
        }
    }
}
