import Foundation
import then
import MapKit

final class MapInteractor {
    let client: HTTPClient
    weak var viewController: MapViewController?
    lazy var geocoder: CLGeocoder = CLGeocoder()

    init(client: HTTPClient = APIClient(),
         viewController: MapViewController) {
        self.client = client
        self.viewController = viewController
    }

    @discardableResult
    func findCoordinate(for address: String?) -> Bool {
        guard let text = address,
            !text.isEmpty else {
                return false
        }

        geocoder.geocodeAddressString(text) { [weak self] (placemarks, error) in
            if let placemarks = placemarks,
                let location = placemarks.first?.location {
                let span = MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)
                let region = MKCoordinateRegion(center: location.coordinate, span: span)
                self?.viewController?.setMapRegion(region)
                return
            }
            self?.viewController?.show(error!.localizedDescription)
        }
        return false
    }

    func get(_ resource: Fixtures.Resource, userLocation: MKUserLocation) {
        viewController?.showLoading()
        let url = Fixtures.url(for: resource)
        let data = try! Data(contentsOf: url)
        let json = try! JSONSerialization.jsonObject(with: data,
                                                     options: [])

        var dataSource: [Fire] = []
        DispatchQueue.global(qos: .background).async {
            if let array = json as? [[String: Any]] {
                array.forEach { json in
                    let fire = Fire(json: json,
                                    userLocation: CLLocation(latitude: userLocation.coordinate.latitude,
                                                             longitude: userLocation.coordinate.longitude))
                    let location = CLLocation(latitude: fire.coordinate.latitude, longitude: fire.coordinate.longitude)
                    CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                        if let placemark = placemarks?.first {
                            fire.name = placemark.country
                        }
                    })
                    dataSource.append(fire)
                }
            } else if let dictionary = json as? [String: Any] {
                dictionary.forEach { key, value in
                    if let value = value as? [String: Any] {
                        let fire = Fire(json: value,
                                        userLocation: CLLocation(latitude: userLocation.coordinate.latitude,
                                                                 longitude: userLocation.coordinate.longitude))
                        let location = CLLocation(latitude: fire.coordinate.latitude, longitude: fire.coordinate.longitude)
                        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                            if let placemark = placemarks?.first {
                                fire.name = placemark.country
                            }
                        })
                        dataSource.append(fire)
                    }
                }
            }
            DispatchQueue.main.async {
                self.viewController?.updateMap(with: dataSource)
                Store.shared.dataSource = dataSource
                self.viewController?.hideLoading()
            }
        }
    }

    func getAll(basedOn userLocation: MKUserLocation) {
        if !Store.shared.dataSource.isEmpty {
            viewController?.updateMap(with: Store.shared.dataSource)
            return
        }

        Store.shared.userLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,
                                                           longitude: userLocation.coordinate.longitude)

        let url = Fixtures.url(for: .fires)
        let data = try! Data(contentsOf: url)
        let json = try! JSONSerialization.jsonObject(with: data,
                                                     options: [])

        var dataSource: [Fire] = []
        let dictionary = json as! [String: Any]
        dictionary.forEach { key, value in
            if let value = value as? [String: Any] {
                let fire = Fire(json: value,
                                userLocation: CLLocation(latitude: userLocation.coordinate.latitude,
                                                         longitude: userLocation.coordinate.longitude))
                let location = CLLocation(latitude: fire.coordinate.latitude, longitude: fire.coordinate.longitude)
                CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                    if let placemark = placemarks?.first {
                        fire.name = placemark.country
                    }
                })
                dataSource.append(fire)
            }
        }
        viewController?.updateMap(with: dataSource)
        Store.shared.dataSource = dataSource

        return
        
        let promise: Promise<[Fire]> = client.request(Fire.Resource.all)
        promise.then { [weak self] fires in
            DispatchQueue.main.async {
                self?.viewController?.updateMap(with: fires)
                Store.shared.dataSource = fires
            }
            }.onError { [weak self] error in
                DispatchQueue.main.async {
                    self?.viewController?.show(error.localizedDescription)
                }
        }
    }
}
