import Foundation

final class Fixtures {
    static func decode<T: Decodable>(resource: Resource) -> T {
        let bundle = Bundle.init(for: Fixtures.self)
        let url = bundle.url(forResource: resource.rawValue,
                             withExtension: "json")
        let data = try! Data(contentsOf: url!)
        return try! JSONDecoder().decode(T.self, from: data)
    }

    static func decode<T: Decodable>(resource: Resource, decoder: JSONDecoder) -> T {
        let bundle = Bundle.init(for: Fixtures.self)
        let url = bundle.url(forResource: resource.rawValue,
                             withExtension: "json")
        let data = try! Data(contentsOf: url!)
        return try! decoder.decode(T.self, from: data)
    }

    static func url(for resource: Resource) -> URL {
        let bundle = Bundle.init(for: Fixtures.self)
        let url = bundle.url(forResource: resource.rawValue,
                             withExtension: "json")
        return url!
    }

    enum Resource: String {
        case fires = "fires"
        case density = "response"
        case brightness
    }
}
