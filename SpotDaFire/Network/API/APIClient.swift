import Foundation

final class APIClient: HTTPClient {
    var baseURL: URL {
        return URL(string: "URL")!
    }
    var manager: SessionManagerInterface

    init(manager: SessionManagerInterface = SessionManager()) {
        self.manager = manager
    }
}
