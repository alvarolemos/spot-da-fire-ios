import Foundation
import then

struct HTTPTarget<R: HTTPResource>: HTTPTargetProtocol {
    let baseURL: URL?
    let resource: R
}

extension HTTPTargetProtocol {
    var URL: URL? {
        return baseURL?.appendingPathComponent(resource.path)
    }
}

// MARK: - Protocols

protocol HTTPClient {
    var baseURL: URL { get }
    var manager: SessionManagerInterface { get }

    init(manager: SessionManagerInterface)
}

protocol HTTPResource {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var useCache: Bool { get }
    var body: Data? { get }
}

protocol HTTPTargetProtocol {
    associatedtype Resource: HTTPResource
    var baseURL: URL? { get }
    var resource: Resource { get }
}

// MARK: Extensions

extension HTTPClient {
    private func request<T: HTTPTargetProtocol>(_ target: T) -> Promise<Data> {
        return manager.request(url: target.URL,
                               method: target.resource.method,
                               params: target.resource.parameters,
                               useCache: target.resource.useCache,
                               body: target.resource.body)
    }

    public func request<R: HTTPResource, T: Decodable>(_ resource: R,
                                                       decoder: JSONDecoder? = nil) -> Promise<T> {
        let target = HTTPTarget(baseURL: baseURL, resource: resource)
        let promise = Promise<T>()
        request(target)
            .then { data in

                var jsonDecoder: JSONDecoder
                if let decoder = decoder {
                    jsonDecoder = decoder
                } else {
                    jsonDecoder = JSONDecoder()
                }

                do {
                    promise.fulfill(try jsonDecoder.decode(T.self, from: data))
                } catch {
                    promise.reject(error)
                }
            }
            .onError { error in
                promise.reject(error)
        }
        return promise
    }

    public func request<R: HTTPResource>(_ resource: R) -> Promise<Data> {
        let target = HTTPTarget(baseURL: baseURL,
                                resource: resource)
        let promise = Promise<Data>()
        request(target)
            .then {
                promise.fulfill($0)
            }.onError {
                promise.reject($0)
        }
        return promise
    }
}

extension HTTPResource {
    var method: HTTPMethod {
        return .get
    }

    var parameters: Parameters? {
        return nil
    }

    var useCache: Bool {
        return true
    }

    var body: Data? {
        return nil
    }
}
