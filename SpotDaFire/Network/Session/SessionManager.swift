import Foundation
import then

// MARK: - Definitions

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

enum SessionManagerError: Error {
    case invalidURL
}

public typealias Parameters = Any

private struct TaskCompletion {
    let taskId: Int?
    let promise: Promise<Data>
    let useCache: Bool
    var responseData: Data
}

extension TaskCompletion: Equatable {
    static func == (lhs: TaskCompletion, rhs: TaskCompletion) -> Bool {
        return lhs.taskId == rhs.taskId
    }
}

// MARK: - Session Manager

protocol SessionManagerInterface {
    func request(url: URL?,
                 method: HTTPMethod,
                 params: Parameters?,
                 useCache: Bool,
                 body: Data?) -> Promise<Data>
}

public final class SessionManager: NSObject, SessionManagerInterface {
    private var session: URLSession?
    private let queue = DispatchQueue(label: "com.spotDaFire.network.queue")
    fileprivate var taskCompletions = [TaskCompletion]()
    lazy var printer: RequestPrinter = RequestPrinter()

    override init() {
        super.init()

        let configuration = URLSessionConfiguration.default
        session = URLSession(configuration: configuration,
                             delegate: self,
                             delegateQueue: nil)
    }

    func request(url: URL?,
                 method: HTTPMethod,
                 params: Parameters?,
                 useCache: Bool,
                 body: Data?) -> Promise<Data> {

        guard let url = url else {
            return Promise.reject(SessionManagerError.invalidURL)
        }

        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        if let parameters = params as? [String: Any], method == .get {
            components?.queryItems = parameters.map({ (key, value) -> URLQueryItem in
                URLQueryItem(name: key, value: value as? String)
            })
        }

        guard let finalURL = components?.url else {
            return Promise.reject(SessionManagerError.invalidURL)
        }

        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue

        if let body = body, method != .get {
            request.httpBody = body
        }

        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

        printer.print(request)

        let promise = Promise<Data>()
        let task = self.session?.dataTask(with: request)
        let taskCompletion = TaskCompletion(taskId: task?.taskIdentifier,
                                            promise: promise,
                                            useCache: useCache,
                                            responseData: Data())

        self.queue.sync {
            self.taskCompletions.append(taskCompletion)
        }
        task?.resume()
        return promise
    }

    func invalidateSession() {
        session?.invalidateAndCancel()
    }

    private func removeTaskFromQueue(_ taskCompletion: TaskCompletion) {
        queue.sync {
            if let index = taskCompletions.index(of: taskCompletion) {
                taskCompletions.remove(at: index)
            }
        }
    }

    private func taskCompletion(by taskId: Int) -> TaskCompletion? {
        var taskCompletion: TaskCompletion?
        queue.sync {
            taskCompletion = self.taskCompletions.first(where: { $0.taskId == taskId })
        }
        return taskCompletion
    }
}

// MARK: - URLSession Delegate

extension SessionManager: URLSessionDataDelegate {

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        queue.sync {
            if let index = taskCompletions.index(where: { $0.taskId == dataTask.taskIdentifier }) {
                taskCompletions[index].responseData.append(data)
            }
        }
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let taskCompletion = taskCompletion(by: task.taskIdentifier) else {
            return
        }

        printer.print(task.response,
                      data: taskCompletion.responseData,
                      error: error)

        if let response = task.response,
            let httpResponse = response as? HTTPURLResponse {
            if httpResponse.statusCode > 299 {
                let error = NSError(domain: "com.spotDaFire", code: 404, userInfo: [NSLocalizedDescriptionKey: "generic-error-messsage".localized])
                taskCompletion.promise.reject(error)
            }
        }

        guard let error = error as NSError? else {
            taskCompletion.promise.fulfill(taskCompletion.responseData)
            removeTaskFromQueue(taskCompletion)
            return
        }

        if error.code != NSURLErrorCancelled,
            taskCompletion.useCache,
            let request = task.currentRequest,
            let cachedResponse = URLCache.shared.cachedResponse(for: request),
            let task = task as? URLSessionDataTask {
            urlSession(session, dataTask: task, didReceive: cachedResponse.data)
            urlSession(session, task: task, didCompleteWithError: nil)
            return
        }

        taskCompletion.promise.reject(error)
        removeTaskFromQueue(taskCompletion)
    }
}
