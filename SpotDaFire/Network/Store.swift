import Foundation
import CoreLocation

/// I hate myself for doing a Singleton, gods know how I hate this, but I'm too tired and it's easy
final class Store {
    lazy var dataSource: [Fire] = []
    lazy var userLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0,
                                                                           longitude: 0)
    static let shared: Store = Store()
    private init() {}
}
