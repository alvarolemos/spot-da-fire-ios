import Foundation

struct RequestPrinter {
    func print(_ request: URLRequest) {
        DispatchQueue.main.async {

            Swift.print("============ 📤 REQUEST ============")
            Swift.print("Method: \(request.httpMethod ?? "")")
            Swift.print(request.debugDescription)
            Swift.print("Headers: \(request.allHTTPHeaderFields ?? ["": ""])")

            if let body = request.httpBody {
                do {
                    let json = try JSONSerialization.jsonObject(with: body, options: .allowFragments)
                    Swift.print("Body: \(json)")
                } catch {
                    Swift.print("Malformed HTTP body")
                }
            }
            Swift.print("====================================")

        }
    }

    func print(_ response: URLResponse?, data: Data?, error: Error?) {
        DispatchQueue.main.async {
            Swift.print("============ 📥 RESPONSE ============")
            Swift.print(response?.debugDescription ?? "")
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    Swift.print("Data:")
                    Swift.print(json)
                } catch {
                    Swift.print("Data is not JSON serializable")
                }
            }
            if let error = error {
                Swift.print("🚨 Error: \(error.localizedDescription)")
            }
            Swift.print("=====================================")
        }
    }
}
