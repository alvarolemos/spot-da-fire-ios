//
//  UIImage+Upload.swift
//  SpotDaFire
//
//  Created by Luiz Henrique Bueno Byrro on 21/10/2018.
//  Copyright © 2018 Jean Sandrin. All rights reserved.
//

import UIKit
import FirebaseStorage
import GoogleSignIn

extension UIImage {
    func uploadImagePic(latLng: (Double,Double), currentTime: String, completion: @escaping (String?) -> Void){
        guard let data = self.jpeg(.medium) else {
            return
        }
        let userID = GIDSignIn.sharedInstance()?.currentUser?.userID ?? "default"
        // set upload path
        let filePath = "\(userID)_\(latLng.0)_\(latLng.1)_\(currentTime)" // path where you wanted to store img in storage
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        
        let storageRef = Storage.storage().reference()
        storageRef.child(filePath).putData(data, metadata: metaData) { (metaData, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
                return
            }else{
                completion(metaData?.name)
            }
        }
    }
}
