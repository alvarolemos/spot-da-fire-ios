import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension UIColor {
    convenience init(hex: String) {
        var trimmedHex = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if trimmedHex.hasPrefix("#") {
            trimmedHex.remove(at: trimmedHex.startIndex)
        }

        if trimmedHex.count != 6 {
            self.init()
            return
        }

        var rgbValue: UInt32 = 0
        Scanner(string: trimmedHex).scanHexInt32(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: 1.0)
    }
}
