import MapKit

class FireAnnotation: MKMarkerAnnotationView {
    static var reuseID: String = "fireAnnotation"

    override init(annotation: MKAnnotation?,
                  reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        canShowCallout = true
        subtitleVisibility = .adaptive
        titleVisibility = .adaptive

        if let fire = annotation as? Fire {
            if let color = fire.color {
                markerTintColor = UIColor(hex: color)
            } else {
                markerTintColor = .red
            }
            clusteringIdentifier = fire.cluster
        }

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForDisplay() {
        super.prepareForDisplay()
        displayPriority = .defaultLow
        glyphImage = #imageLiteral(resourceName: "fire")
    }
}
