import MapKit

final class FireClusterAnnotation: MKAnnotationView {
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        collisionMode = .circle
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForDisplay() {
        super.prepareForDisplay()
        guard let cluster = annotation as? MKClusterAnnotation else {
            return
        }

        let total = cluster.memberAnnotations.count
        let color: UIColor
        if let fire = cluster.memberAnnotations.first as? Fire,
            let fireColor = fire.color {
            color = UIColor(hex: fireColor)
        } else {
            color = .red
        }
        image = drawRatio(fraction: 0,
                          total: total,
                          fractionColor: .yellow,
                          fillColor: color)
        displayPriority = .defaultHigh
    }

    private func count(kind: Fire.Kind) -> Int {
        guard let cluster = annotation as? MKClusterAnnotation else {
            return 0
        }


        return cluster.memberAnnotations.compactMap { $0 as? Fire }.filter { $0.kind == kind }.count
    }

    func drawRatio(fraction: Int,
                   total: Int,
                   fractionColor: UIColor,
                   fillColor: UIColor) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: 20, height: 20))
        return renderer.image { _ in
            fillColor.setFill()
            UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 20, height: 20)).fill()

            fractionColor.setFill()
            let path = UIBezierPath()
            path.addArc(withCenter: CGPoint(x: 10, y: 10), radius: 10,
                        startAngle: 0, endAngle: (CGFloat.pi * 2.0 * CGFloat(fraction)) / CGFloat(total),
                        clockwise: true)
            path.addLine(to: CGPoint(x: 10, y: 10))
            path.close()
            path.fill()

            UIColor.white.setFill()
            UIBezierPath(ovalIn: CGRect(x: 4, y: 4, width: 12, height: 12)).fill()

            let attributes: [NSAttributedString.Key : Any] = [.foregroundColor: UIColor.black,
                                                              .font: UIFont.boldSystemFont(ofSize: 10)]
            let text = "\(total)"
            let size = text.size(withAttributes: attributes)
            let rect = CGRect(x: 10 - size.width / 2,
                              y: 10 - size.height / 2,
                              width: size.width, height: size.height)
            text.draw(in: rect, withAttributes: attributes)
        }
    }
}
